package mmm.movies;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import mmm.movies.Beans.MovieCard;

public class MoviesDataSource {

    public static final String TABLE_NAME="FILMS";
    public static final String STRING_TYPE="TEXT";
    public static final String INT_TYPE="INTEGER";

    public class ColumnPeliculas{
        public static final String ID_TABLE="_id";
        public static final String ID_MOVIE="ID";
        public static final String POSTER_PATH="POSTER";
        public static final String TITLE_MOVIE="TITLE";
        public static final String YEAR_MOVIE="YEAR";
        public static final String IS_SEEN="IS_SEEN";
        public static final String IS_WISH="IS_WISH";

    }
    public static final String CREATE_MOVIES_SCRIPT ="CREATE TABLE "+TABLE_NAME+"("+
            ColumnPeliculas.ID_TABLE+" "+INT_TYPE+" PRIMARY KEY, "+
            ColumnPeliculas.ID_MOVIE+" "+INT_TYPE+" NOT NULL, "+
            ColumnPeliculas.TITLE_MOVIE+" "+STRING_TYPE+" NOT NULL, "+
            ColumnPeliculas.YEAR_MOVIE+" "+INT_TYPE+", "+
            ColumnPeliculas.POSTER_PATH+" "+STRING_TYPE+", "+
            ColumnPeliculas.IS_SEEN+" "+INT_TYPE+", "+
            ColumnPeliculas.IS_WISH+" "+INT_TYPE+");";

    public static final String SELECT_SEEN_MOVIES = "SELECT * FROM "+
            TABLE_NAME+ " WHERE "+ColumnPeliculas.IS_SEEN+" = 1;";

    public static final String SELECT_WISH_MOVIES = "SELECT * FROM "+
            TABLE_NAME+ " WHERE "+ColumnPeliculas.IS_WISH+" = 1;";


    private MoviesSQLiteHelper openHelper;
    private SQLiteDatabase database;

    public MoviesDataSource(Context context){
        openHelper=MoviesSQLiteHelper.getInstance(context);
        database=openHelper.getWritableDatabase();
    }

    public void InsertMovie(int id, String title,String year,String poster_path, int is_seen, int is_wish){
        database.execSQL("INSERT INTO "+TABLE_NAME+" VALUES"+
                " (NULL,"+id+",'"+title+"','"+year+"','"+poster_path+"',"+is_seen+","+is_wish+");");
    }
    public void BorrarPelicula(int id){
        database.execSQL("DELETE FROM "+TABLE_NAME+
                " WHERE "+ ColumnPeliculas.ID_MOVIE+" = "+id+";");
    }
    public void Actualizar(int id, String tipo, int nuevo){
        database.execSQL("UPDATE "+TABLE_NAME+" SET "+tipo+"="+nuevo+
                " WHERE "+ ColumnPeliculas.ID_MOVIE+" = "+id+";");
    }
    public Cursor getSeenMovies(){
        return database.rawQuery(SELECT_SEEN_MOVIES, null);
    }
    public Cursor getWishMovies(){
        return database.rawQuery(SELECT_WISH_MOVIES, null);
    }
    /*
    public Cursor getPeliculas(){
        return database.rawQuery(SELECT_MOVIE_SCRIPT, null);
    }
    public Cursor getVistas(){
        return database.rawQuery(SELECT_VISTAS_SCRIPT, null);
    }
    public Cursor getFavoritas(){
        return database.rawQuery(SELECT_FAVORITAS_SCRIPT, null);
    }
    public Cursor getEstrenos(){
        return database.rawQuery(SELECT_ESTRENOS_SCRIPT, null);
    }
    */


}
