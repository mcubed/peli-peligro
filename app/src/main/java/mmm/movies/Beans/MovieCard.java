package mmm.movies.Beans;


import java.text.SimpleDateFormat;

public class MovieCard {

    private int id;
    private String title;
    private String year;
    private String poster_path;
    private String status;

    public MovieCard(Movie movie){

        this.poster_path=movie.getPoster_path();
        this.id=movie.getId();
        this.title=movie.getTitle();
        this.year = convertToYear(movie.getRelease_date());

    }

    public MovieCard(int id, String title, String year, String poster_path, String status) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.poster_path = poster_path;
        this.status = status;
    }


    public String convertToYear(String date){
        String year="";

        if(!date.equals(""))
        for (int i = 0; i < 4; i++){

            year += date.charAt (i);
        }
        return year;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public String getYear() {
        return year;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
