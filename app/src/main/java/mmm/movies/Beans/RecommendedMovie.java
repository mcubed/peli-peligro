package mmm.movies.Beans;

public class RecommendedMovie{
    private int accuracy;
    private Movie movie;

    public RecommendedMovie(int accuracy, Movie movie) {
        this.accuracy = accuracy;
        this.movie = movie;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void increaseAccuracy(){
        this.accuracy++;
    }

    public Movie getMovie() {
        return movie;
    }

    public boolean compare(Movie movie){
        boolean same;
        if(same=this.movie == movie) {
            accuracy++;
        }
        return same;
    }
}
