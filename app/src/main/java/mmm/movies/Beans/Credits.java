
package mmm.movies.Beans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Credits {

    private Integer id;
    private List<Actor> cast = null;
    private List<Worker> crew = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Actor> getCast() {
        return cast;
    }

    public void setCast(List<Actor> cast) {
        this.cast = cast;
    }

    public List<Worker> getCrew() {
        return crew;
    }

    public void setCrew(List<Worker> crew) {
        this.crew = crew;
    }


}
