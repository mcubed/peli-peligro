package mmm.movies;


import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import mmm.movies.Beans.Responses;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    TextView text;
    VolleySingleton volley;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        text=(TextView)view.findViewById(R.id.text);

        volley = VolleySingleton.getInstance(getContext());


        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_home, menu);

        // Configiracion para el SearchView
        configurarSearchView(menu);

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void configurarSearchView(Menu menu){
        SearchManager searchManager;
        SearchView searchView;
        try {
            searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            searchView = (SearchView) menu.findItem(R.id.search_item).getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
            searchView.setIconifiedByDefault(false);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    searchMovies(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });
        }catch (Exception e){

        }finally{
            searchManager=null;
            searchView=null;
        }
    }

    public void searchMovies(String query){

        String url=Constants.URL_SEARCH+"&query="+removeSpaces(query)+Constants.LANGUAGE+Constants.ES_LANGUAGE;
        Request request;


        request = new GsonRequest<>(url, Responses.class, null,
                new Response.Listener<Responses>() {
                    @Override
                    public void onResponse(Responses response) {

                        startSearchRecyclerActivity(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                    text.setText("Error");
            }
        });

        //Añadimos la petición a la cola
        volley.addToRequestQueue(request);
    }

    public void startSearchRecyclerActivity(Responses responses){

        Intent intent;
        Bundle bundle;

        try {
            intent = new Intent(getActivity(), SearchRecyclerActivity.class);
            bundle = new Bundle();

            bundle.putSerializable("RESPONSES", responses);
            intent.putExtras(bundle);
            startActivity(intent);

        }catch(Exception e){

            e.printStackTrace();

        }finally{
            intent=null;
            bundle=null;
        }
    }
    public String removeSpaces(String cadena){

        String cadNoSpaces = "";
        for(int i = 0; i< cadena.length(); i++){

            if(cadena.charAt(i) != ' ')
                cadNoSpaces += cadena.charAt(i);
            else
                cadNoSpaces += "%20";
        }
        return cadNoSpaces;
    }


}
