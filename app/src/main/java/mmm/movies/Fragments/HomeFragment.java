package mmm.movies.Fragments;


import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SearchView;


import java.util.ArrayList;
import java.util.List;


import mmm.movies.Beans.MovieCard;
import mmm.movies.ClickListener;
import mmm.movies.Constants;
import mmm.movies.R;
import mmm.movies.Adapters.MovieSmallAdapter;
import mmm.movies.SQLite.MoviesDataSource;
import mmm.movies.SearchRecyclerActivity;
import mmm.movies.VolleySingleton;


public class HomeFragment extends Fragment {


    VolleySingleton volley;
    RecyclerView wishRecyclerView;
    RecyclerView recommendRecyclerView;
    RelativeLayout noWishLayout;
    RelativeLayout noRecoLayout;

    private MoviesDataSource dataSource;

    private List<MovieCard> wishMovies = new ArrayList<>();
    private List<MovieCard> recommendedMovies = new ArrayList<>();


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);


        wishRecyclerView = (RecyclerView) view.findViewById(R.id.wish_recycler_view);
        recommendRecyclerView = (RecyclerView) view.findViewById(R.id.recom_recycler_view);

        noWishLayout = (RelativeLayout) view.findViewById(R.id.no_wish_films_layout);
        noRecoLayout = (RelativeLayout) view.findViewById(R.id.no_reco_films_layout);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);

        wishRecyclerView.setLayoutManager(layoutManager);
        recommendRecyclerView.setLayoutManager(layoutManager2);

        volley = VolleySingleton.getInstance(getContext());

        dataSource = new MoviesDataSource(getContext());

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_home, menu);

        // Configiracion para el SearchView
        configurarSearchView(menu);

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();

        wishMovies = dataSource.getSampleMovieCardList(Constants.WISH);
        if(!wishMovies.isEmpty()) {
            noWishLayout.setVisibility(View.GONE);
            MovieSmallAdapter wishMovieAdapter = new MovieSmallAdapter(getContext(), wishMovies);
            ClickListener clickListener = new ClickListener(getContext(), wishMovieAdapter, dataSource);
            wishMovieAdapter.setListener(clickListener);
            wishRecyclerView.setAdapter(wishMovieAdapter);
        }

        recommendedMovies = dataSource.getSampleMovieCardList(Constants.REC0);
        if(!recommendedMovies.isEmpty()) {
            noRecoLayout.setVisibility(View.GONE);
            MovieSmallAdapter recommendedMovieAdapter = new MovieSmallAdapter(getContext(), recommendedMovies);
            ClickListener clickListener = new ClickListener(getContext(), recommendedMovieAdapter, dataSource);
            recommendedMovieAdapter.setListener(clickListener);
            recommendRecyclerView.setAdapter(recommendedMovieAdapter);
        }

    }

    public void configurarSearchView(Menu menu){
        SearchManager searchManager;
        SearchView searchView;
        try {
            searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            searchView = (SearchView) menu.findItem(R.id.search_item).getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
            searchView.setIconifiedByDefault(false);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    startSearchRecyclerActivity(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });
        }catch (Exception e){

        }finally{
            searchManager=null;
            searchView=null;
        }
    }


    public void startSearchRecyclerActivity(String query){

        Intent intent;
        Bundle bundle;

        try {
            intent = new Intent(getActivity(), SearchRecyclerActivity.class);
            bundle = new Bundle();

            bundle.putSerializable(Constants.QUERY, query);
            intent.putExtras(bundle);
            startActivity(intent);

        }catch(Exception e){

            e.printStackTrace();

        }
    }

}
