package mmm.movies.Fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mmm.movies.API.TMDBService;
import mmm.movies.API.TheMovieDB;
import mmm.movies.Beans.Movie;
import mmm.movies.Beans.MovieCard;
import mmm.movies.Beans.Responses;
import mmm.movies.Constants;
import mmm.movies.ClickListener;
import mmm.movies.MovieActivity;
import mmm.movies.R;
import mmm.movies.Adapters.MovieAdapter;
import mmm.movies.SQLite.MoviesDataSource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RecommendationsFragment extends Fragment {


    private MoviesDataSource dataSource;
    private TheMovieDB database;

    private List<Movie> recommendedMovies = new ArrayList<>();
    private List<Integer> seenMovieIds;
    private Iterator<Integer> seenMovieIdsIterator;

    private RecyclerView recyclerView;

    private ProgressDialog progressDialog;


    public RecommendationsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dataSource = new MoviesDataSource(getContext());
        database = TMDBService.getInstance();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list_movies, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        final MovieAdapter movieAdapter = new MovieAdapter(getContext(), dataSource.getMovieCardList(Constants.REC0));
        ClickListener clickListener = new ClickListener(getContext(), movieAdapter, dataSource);
        movieAdapter.setListener(clickListener);
        recyclerView.setAdapter(movieAdapter);

        ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);


        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_recommended, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.update_item) {
            showProgressDialog();
            return obtainRecommendedMovie();
        }else
            return false;
    }

    private void showProgressDialog() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Obteniendo recomendaciones ...");
        progressDialog.show();
    }

    public boolean obtainRecommendedMovie(){

        seenMovieIds = dataSource.getMovieIds(Constants.SEEN);
        seenMovieIdsIterator=seenMovieIds.iterator();

        if(seenMovieIdsIterator.hasNext())
            loadRecommendations(seenMovieIdsIterator.next());

        return true;

    }
    public List<MovieCard> getMovieCards(){

        List<Integer> listedMovies = new ArrayList<>();
        listedMovies.addAll(seenMovieIds);
        listedMovies.addAll(dataSource.getMovieIds(Constants.WISH));
        listedMovies.addAll(dataSource.getMovieIds(Constants.SHIT));

        List<MovieCard> movieCards = new ArrayList<>();
        int maxAccuracy=seenMovieIds.size();
        while(movieCards.size()<20) {
            for (Movie recommendedMovie : recommendedMovies) {
                int frequency = recommendedMovie.frequency(recommendedMovies);
                if (frequency == maxAccuracy
                        && !listedMovies.contains(recommendedMovie.getId())
                        && !recommendedMovie.belongTo(movieCards)){
                    MovieCard newRecommendedMovie = new MovieCard(recommendedMovie);
                    newRecommendedMovie.setStatus(Constants.REC0);
                    movieCards.add(newRecommendedMovie);
                    dataSource.insertMovie(newRecommendedMovie);
                }
            }
            maxAccuracy--;
        }
        return movieCards;
    }

    public void loadRecommendations(int idMovie){
        Call<Responses> responsesCall = database.getRecommendations(idMovie);
        responsesCall.enqueue(new RecommendationsCallback());
    }

    public void setRecommendedMovie(List<Movie> newRecommendations){

        for (Movie movie : newRecommendations) {
            recommendedMovies.add(movie);
        }

    }

    public void setAdapter(){
        final MovieAdapter movieAdapter;
        if(!recommendedMovies.isEmpty()){
            dataSource.deleteMovies(Constants.REC0);
            movieAdapter = new MovieAdapter(getContext(), getMovieCards());
            ClickListener clickListener = new ClickListener(getContext(), movieAdapter, dataSource);
            movieAdapter.setListener(clickListener);
            recyclerView.setAdapter(movieAdapter);
        }else{

        }
    }
    public void startMovieActivity(int movieId){
        Intent goToMovieIntent = new Intent(getActivity(), MovieActivity.class);
        goToMovieIntent.putExtra(Constants.ID, movieId);
        startActivity(goToMovieIntent);
    }

    private class RecommendationsCallback implements Callback<Responses>{

        @Override
        public void onResponse(Call<Responses> call, Response<Responses> response) {
            setRecommendedMovie(response.body().getResults());
            if(seenMovieIdsIterator.hasNext())
                loadRecommendations(seenMovieIdsIterator.next());
            else{
                setAdapter();
                progressDialog.closeOptionsMenu();
                progressDialog.dismiss();
            }
        }

        @Override
        public void onFailure(Call<Responses> call, Throwable t) {

        }
    }


}
