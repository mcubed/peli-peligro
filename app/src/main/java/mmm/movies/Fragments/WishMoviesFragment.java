package mmm.movies.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import mmm.movies.ClickListener;
import mmm.movies.Constants;
import mmm.movies.R;
import mmm.movies.Adapters.MovieAdapter;
import mmm.movies.SQLite.MoviesDataSource;


public class WishMoviesFragment extends Fragment {


    private MoviesDataSource dataSource;

    public WishMoviesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        dataSource = new MoviesDataSource(getContext());

        View view = inflater.inflate(R.layout.fragment_list_movies, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        MovieAdapter movieAdapter = new MovieAdapter(getContext(), dataSource.getMovieCardList(Constants.WISH));
        ClickListener clickListener = new ClickListener(getContext(), movieAdapter, dataSource);
        movieAdapter.setListener(clickListener);
        recyclerView.setAdapter(movieAdapter);

        ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);

        return view;
    }

}
