package mmm.movies;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

import mmm.movies.Adapters.MovieSmallAdapter;
import mmm.movies.Beans.MovieCard;
import mmm.movies.Adapters.MovieAdapter;
import mmm.movies.SQLite.MoviesDataSource;


public class ClickListener {

    private MovieAdapter movieAdapter;
    private MovieSmallAdapter movieSmallAdapter;
    private MoviesDataSource dataSource;
    private Context context;

    public ClickListener(Context context, MovieAdapter movieAdapter, MoviesDataSource dataSource) {
        this.movieAdapter = movieAdapter;
        this.dataSource = dataSource;
        this.context = context;
    }

    public ClickListener(Context context, MovieSmallAdapter movieSmallAdapter, MoviesDataSource dataSource) {
        this.movieSmallAdapter = movieSmallAdapter;
        this.dataSource = dataSource;
        this.context = context;
    }

    public void onClick(int viewId, int position){
        MovieCard currentMovie = movieAdapter.getMovieCardAtPosition(position);
        switch (viewId){
            case R.id.add_seen:
                currentMovie = dataSource.saveStatus(currentMovie, Constants.SEEN);
                break;
            case R.id.add_whish:
               currentMovie = dataSource.saveStatus(currentMovie, Constants.WISH);
                break;
            default:
                startMovieActivity(currentMovie.getId());
        }
        movieAdapter.setMovieCardAtPosition(currentMovie,position);
        movieAdapter.notifyItemChanged(position);

    }
    public void onClick(int position){

        MovieCard currentMovie = movieSmallAdapter.getMovieCardAtPosition(position);
        startMovieActivity(currentMovie.getId());

    }

    public void onLongClick(int position){
        MovieCard currentMovie = movieAdapter.getMovieCardAtPosition(position);
        deleteMovieDialog(currentMovie.getId(), position);
    }
    public void startMovieActivity(int movieId){
        Intent goToMovieIntent = new Intent(context, MovieActivity.class);
        goToMovieIntent.putExtra(Constants.ID, movieId);
        goToMovieIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(goToMovieIntent);
    }

    public void deleteMovieDialog(final int movieId, final int position){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Eliminar pelicula")
                .setPositiveButton("eliminar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dataSource.updateMovieStatus(movieId, Constants.SHIT);
                        movieAdapter.removeMovie(position);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("calcelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setMessage("¿Quiere eliminar esta pelicula para que no le aparezca más?");
        AlertDialog dialog = builder.create();
        dialog.show();

    }

}
