package mmm.movies;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.solver.widgets.ConstraintAnchor;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import mmm.movies.API.TMDBService;
import mmm.movies.API.TheMovieDB;
import mmm.movies.Adapters.CastAdapter;
import mmm.movies.Adapters.CrewAdapter;
import mmm.movies.Adapters.MovieAdapter;
import mmm.movies.Beans.Credits;
import mmm.movies.Beans.Movie;
import mmm.movies.Beans.MovieCard;
import mmm.movies.Beans.Responses;
import mmm.movies.SQLite.MoviesDataSource;
import retrofit2.Call;
import retrofit2.Callback;

public class MovieActivity extends AppCompatActivity {


    private Movie movie;
    private TheMovieDB database;
    private MoviesDataSource dataSource;

    private ImageView imgPoster;
    private TextView textOriginalTitle;
    private TextView textDate;
    private TextView textLanguage;
    private TextView textOverview;
    private TextView textDuration;
    private FloatingActionButton fabWish;
    private FloatingActionButton fabSeen;

    private RecyclerView recyclerViewCast;
    private RecyclerView recyclerViewCrew;


    private CollapsingToolbarLayout ctlLayout;
    private AppBarLayout appBarLayout;
    private ConstraintLayout constraintLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        appBarLayout = (AppBarLayout)findViewById(R.id.appbarLayout);
        constraintLayout = (ConstraintLayout)findViewById(R.id.constraint);
        ctlLayout = (CollapsingToolbarLayout)findViewById(R.id.ctlLayout);
        imgPoster = (ImageView) findViewById(R.id.movie_poster);
        textOriginalTitle = (TextView) findViewById(R.id.movie_original_title);
        textDate = (TextView) findViewById(R.id.movie_date);
        textLanguage = (TextView) findViewById(R.id.movie_language);
        textOverview = (TextView) findViewById(R.id.movie_overview);
        textDuration = (TextView) findViewById(R.id.duration);
        fabWish = (FloatingActionButton) findViewById(R.id.add_whish);
        fabSeen = (FloatingActionButton) findViewById(R.id.add_seen);


        recyclerViewCast = (RecyclerView)findViewById(R.id.cast_recycler_view);
        recyclerViewCrew = (RecyclerView)findViewById(R.id.crew_recycler_view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewCast.setLayoutManager(layoutManager);

        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(this);
        recyclerViewCrew.setLayoutManager(layoutManager2);


        database = TMDBService.getInstance();
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (appBarLayout.getHeight() / 2 < -verticalOffset) {
                    constraintLayout.setVisibility(View.GONE);
                } else {
                    constraintLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        dataSource = new MoviesDataSource(this);

        fabSeen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStatus(Constants.SEEN);
                dataSource.saveStatus(new MovieCard(movie), Constants.SEEN);
            }
        });

        fabWish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStatus(Constants.WISH);
                dataSource.saveStatus(new MovieCard(movie), Constants.WISH);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        loadMovie(getIntent().getIntExtra(Constants.ID, 0));

    }
    public void setStatus(String status){
        if(status!=null) {
            fabSeen.setSelected(status.equals(Constants.SEEN));
            fabWish.setSelected(status.equals(Constants.WISH));
        }
        else{
            fabSeen.setSelected(false);
            fabWish.setSelected(false);
        }
    }

    public void loadMovie(int idMovie){

        Call<Movie> peticion = database.getMovie(idMovie);
        peticion.enqueue(new MovieCallback());
    }

    public void setMovie(Movie movie){

        String poster_url=movie.getPoster_path();
        if(poster_url != null)
            Picasso.with(this).load(Constants.URL_IMAGE+poster_url)
                    .resize(120, 150)
                    .transform(new RoundedTransformation(20,0))
                    .into(imgPoster);
        ctlLayout.setTitle(movie.getTitle());
        textOriginalTitle.setText(movie.getOriginal_title());
        textDate.setText(movie.getRelease_date());
        textLanguage.setText("["+movie.getOriginal_language()+"]");
        textOverview.setText(movie.getOverview());
        textDuration.setText(movie.getRuntime()+" min");

        setStatus(dataSource.getMovieStatus(String.valueOf(movie.getId())));

        CastAdapter castAdapter = new CastAdapter(getParent(), movie.getCredits().getCast());
        recyclerViewCast.setAdapter(castAdapter);

        CrewAdapter crewAdapter = new CrewAdapter(getParent(), movie.getCredits().getCrew());
        recyclerViewCrew.setAdapter(crewAdapter);
    }
    private class MovieCallback implements Callback<Movie> {


        @Override
        public void onResponse(Call<Movie> call, retrofit2.Response<Movie> response) {
            movie = response.body();
            if(movie!=null)
                setMovie(movie);
            else{
                finish();
            }
        }

        @Override
        public void onFailure(Call<Movie> call, Throwable t) {
        }
    }

}
