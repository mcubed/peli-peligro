package mmm.movies;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import mmm.movies.API.TMDBService;
import mmm.movies.API.TheMovieDB;
import mmm.movies.Beans.Movie;
import mmm.movies.Beans.MovieCard;
import mmm.movies.Beans.Responses;
import mmm.movies.Adapters.MovieAdapter;
import mmm.movies.SQLite.MoviesDataSource;
import retrofit2.Call;
import retrofit2.Callback;

public class SearchRecyclerActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private MoviesDataSource dataSource;
    private TheMovieDB database;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_recycler);

        agregarToolbar();

        database = TMDBService.getInstance();

        dataSource = new MoviesDataSource(this);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        searchMovies(getIntent().getStringExtra(Constants.QUERY));

    }

    public void startMovieActivity(int movieId){
        Intent goToMovieIntent = new Intent(SearchRecyclerActivity.this, MovieActivity.class);
        goToMovieIntent.putExtra(Constants.ID, movieId);
        startActivity(goToMovieIntent);
    }

    public void searchMovies(String query){

        Call<Responses> peticion = database.searchMovie(query);
        peticion.enqueue(new ResponseCallback());

    }



    private void agregarToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public List<MovieCard> getCards(List<Movie> movies){

        List<MovieCard> movieCards=new ArrayList<>();
        List<MovieCard> savedMovieCards = dataSource.getMovieCardList(Constants.ALL);

        for(int i=0; i<movies.size(); i++) {

            Movie currentNewMovie = movies.get(i);
            boolean found = false;
            for(int j=0; j<savedMovieCards.size() && !found; j++){
                MovieCard currentSavedMovie = savedMovieCards.get(j);
                if(currentNewMovie.getId()==currentSavedMovie.getId()) {
                    movieCards.add(currentSavedMovie);
                    found = true;
                }
            }
            if(!found){
                movieCards.add(new MovieCard(currentNewMovie));
            }

        }
        return movieCards;
    }
    private class ResponseCallback implements Callback<Responses> {


        @Override
        public void onResponse(Call<Responses> call, retrofit2.Response<Responses> response) {

            MovieAdapter movieAdapter = new MovieAdapter(getApplicationContext(),getCards(response.body().getResults()));
            ClickListener listener = new ClickListener(getApplicationContext(), movieAdapter, dataSource);
            movieAdapter.setListener(listener);
            recyclerView.setAdapter(movieAdapter);

        }

        @Override
        public void onFailure(Call<Responses> call, Throwable t) {

        }
    }
}
