package mmm.movies;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;

import mmm.movies.Fragments.HomeFragment;
import mmm.movies.Fragments.RecommendationsFragment;
import mmm.movies.Fragments.SeenMoviesFragment;
import mmm.movies.Fragments.WebViewFragment;
import mmm.movies.Fragments.WishMoviesFragment;


public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private NavigationView navView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_nav_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navView = (NavigationView) findViewById(R.id.navview);

        if (navView != null) {
            prepararDrawer(navView);
        }

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        seleccionarItem(item);
                        return true;
                    }

                });
        bottomNavigationView.setSelectedItemId(R.id.menu_home);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
    }

    private void prepararDrawer(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        seleccionarItem(menuItem);
                        drawerLayout.closeDrawers();
                        return true;
                    }
                });
    }

    private void seleccionarItem(MenuItem menuItem) {

        Fragment fragment = null;

        switch (menuItem.getItemId()) {
            case R.id.menu_home:
                fragment=new HomeFragment();
                break;
            case R.id.menu_seen:
                fragment = new SeenMoviesFragment();
                break;
            case R.id.menu_wish:
                fragment = new WishMoviesFragment();
                break;
            case R.id.menu_recommendations:
                fragment=new RecommendationsFragment();
                break;
            case R.id.menu_taste:
                fragment=new WebViewFragment();
                break;

        }

        if (fragment!=null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contenedor_principal, fragment)
                    .commit();

            getSupportActionBar().setTitle(menuItem.getTitle());
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

}
