package mmm.movies;

public interface Constants {

    //API
    String API_KEY="b9288001dc9d30cc9009b3ec908a2691";
    String URL_BASE="https://api.themoviedb.org/3";
    String URL_KEY="api_key="+API_KEY;
    String URL_MOVIE="/movie/";
    String URL_SEARCH=URL_BASE+"/search/movie?"+URL_KEY;
    String URL_IMAGE="http://image.tmdb.org/t/p/w342";
    String LANGUAGE="&language=";
    String APPEND="&append_to_response=";
    String ES_LANGUAGE="es";

    String QUERY="query";
    String ID="id";


    String SEEN = "seen";
    String WISH = "wish";
    String REC0 = "recommended";
    String SHIT = "shit";
    String ALL = "all";


}
