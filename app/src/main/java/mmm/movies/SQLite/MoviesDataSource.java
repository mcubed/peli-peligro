package mmm.movies.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import mmm.movies.Beans.MovieCard;
import mmm.movies.Constants;
import mmm.movies.SQLite.MoviesSQLiteHelper;

public class MoviesDataSource {

    public static final String TABLE_NAME="FILMS";
    public static final String STRING_TYPE="TEXT";
    public static final String INT_TYPE="INTEGER";


    public static final String ID_TABLE="_id";
    public static final String ID_MOVIE="ID";
    public static final String POSTER_PATH="POSTER";
    public static final String TITLE_MOVIE="TITLE";
    public static final String YEAR_MOVIE="YEAR";
    public static final String STATUS="STATUS";


    public static final String CREATE_MOVIES_SCRIPT ="CREATE TABLE "+TABLE_NAME+"("+
            ID_TABLE+" "+INT_TYPE+" PRIMARY KEY, "+
            ID_MOVIE+" "+INT_TYPE+" NOT NULL UNIQUE, "+
            TITLE_MOVIE+" "+STRING_TYPE+" NOT NULL, "+
            YEAR_MOVIE+" "+INT_TYPE+", "+
            POSTER_PATH+" "+STRING_TYPE+", "+
            STATUS+" "+STRING_TYPE+");";




    private MoviesSQLiteHelper openHelper;
    private SQLiteDatabase database;

    public MoviesDataSource(Context context){
        openHelper=MoviesSQLiteHelper.getInstance(context);
        database=openHelper.getWritableDatabase();
    }

    public long insertMovie(MovieCard movie){


            ContentValues newMovie = new ContentValues();

            newMovie.put(ID_MOVIE, movie.getId());
            newMovie.put(TITLE_MOVIE, movie.getTitle());
            newMovie.put(YEAR_MOVIE, movie.getYear());
            newMovie.put(POSTER_PATH, movie.getPoster_path());
            newMovie.put(STATUS, movie.getStatus());

            return database.insert(TABLE_NAME, null, newMovie);

    }
    public void deleteMovie(int id){

        database.delete(TABLE_NAME,ID_MOVIE+"="+id,null);
    }

    public void deleteMovies(String status){

        String values[] = {status};
        database.delete(TABLE_NAME,STATUS+"=?",values);
    }
    public void updateMovieStatus(int id, String status){

        ContentValues values = new ContentValues();
        values.put(STATUS, status);
        database.update(TABLE_NAME,values,ID_MOVIE+"="+id,null);

    }

    public MovieCard saveStatus(MovieCard movie, String status){

        String currentStatus = getMovieStatus(String.valueOf(movie.getId()));

        if(currentStatus==null) {

            movie.setStatus(status);
            insertMovie(movie);

        }else if(status.equals(currentStatus)){

            movie.setStatus(null);
            deleteMovie(movie.getId());

        }else{

            movie.setStatus(status);
            updateMovieStatus(movie.getId(), status);
        }
        return movie;
    }

    public String getMovieStatus(String id){

        Cursor cursor = null;
        String columns[] = {STATUS};
        String values[] = {id};
        String status = null;

        if(id!=null){
            cursor=database.query(true, TABLE_NAME, columns, ID_MOVIE+"=?", values, null, null, null, null);
        }
        if(cursor.getCount()==1){
            cursor.moveToFirst();
            status = cursor.getString(0);
        }
        return status;
    }

    public List<MovieCard> getMovieCardList(String status){

        List<MovieCard> movieCards = new ArrayList<>();
        Cursor cursor;
        String values[] = {status};

        if(status.equals(Constants.ALL)){
            cursor=database.query(true, TABLE_NAME, null, null, null, null, null, null, null);
        }else {
            cursor=database.query(true, TABLE_NAME, null, STATUS + "=?", values, null, null, null, null);
        }

        if(cursor!=null) {

            int id = cursor.getColumnIndex(ID_MOVIE);
            int title = cursor.getColumnIndex(TITLE_MOVIE);
            int year = cursor.getColumnIndex(YEAR_MOVIE);
            int poster = cursor.getColumnIndex(POSTER_PATH);
            int state = cursor.getColumnIndex(STATUS);


            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

                movieCards.add(new MovieCard(
                        cursor.getInt(id),
                        cursor.getString(title),
                        cursor.getString(year),
                        cursor.getString(poster),
                        cursor.getString(state)));
            }
        }
        cursor.close();
        return movieCards;

    }
    public List<MovieCard> getSampleMovieCardList(String status){

        List<MovieCard> allMovies = getMovieCardList(status);
        List<MovieCard> sampleMovies = new ArrayList<>();
        Random r = new Random();

        if(allMovies.size()>5) {
            for (int i = 0; i <= 5; i++) {
                sampleMovies.add(allMovies.remove(r.nextInt(allMovies.size())));
            }

            return sampleMovies;
        }else{
            return allMovies;
        }
    }

    public List<Integer> getMovieIds(String status){

        List<Integer> movieIds = new ArrayList<>();
        Cursor cursor;

        String values[] = {status};
        String columns[] = {ID_MOVIE};

        if(status.equals(Constants.ALL)){
            cursor=database.query(true, TABLE_NAME, columns, null, null, null, null, null, null);
        }else {
            cursor=database.query(true, TABLE_NAME, columns, STATUS+"=?",values, null, null, null, null);
        }


        if(cursor!=null) {

            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

                movieIds.add(cursor.getInt(cursor.getColumnIndex(ID_MOVIE)));
            }
        }

        return movieIds;

    }



}
