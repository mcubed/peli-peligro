package mmm.movies.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MoviesSQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME="films.db";
    private static final int DATABASE_VERSION=1;

    private static MoviesSQLiteHelper dbOpen;

    private MoviesSQLiteHelper(Context contexto){
        super(contexto,DATABASE_NAME,null,DATABASE_VERSION);
        dbOpen=this;
    }

    public static MoviesSQLiteHelper getInstance(Context contexto){
        if(dbOpen==null){
            new MoviesSQLiteHelper(contexto);
        }
        return dbOpen;
    }

    public void onCreate(SQLiteDatabase db){
        db.execSQL(MoviesDataSource.CREATE_MOVIES_SCRIPT);
    }
    public void onUpgrade(SQLiteDatabase db, int versionAnterior,int versionNueva){

    }

}