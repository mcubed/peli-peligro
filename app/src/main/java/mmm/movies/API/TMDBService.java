package mmm.movies.API;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TMDBService {

    private static TheMovieDB servicio = null;

    public synchronized static TheMovieDB getInstance() {
        if(servicio == null){
            new TMDBService();
        }
        return servicio;
    }

    private TMDBService() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TheMovieDB.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        servicio = retrofit.create(TheMovieDB.class);
    }
}
