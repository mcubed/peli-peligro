package mmm.movies.API;

import mmm.movies.Beans.Credits;
import mmm.movies.Beans.Movie;
import mmm.movies.Beans.Responses;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by manu on 8/7/17.
 */

public interface TheMovieDB {

    String API_KEY="b9288001dc9d30cc9009b3ec908a2691";
    String URL_BASE="https://api.themoviedb.org/3/";
    String URL_KEY="?api_key="+API_KEY;
    String URL_MOVIE="movie/";
    String URL_CREDITS="credits";
    String URL_RECOMMENDATIONS="/recommendations";
    String URL_SEARCH="search/movie"+URL_KEY;
    String URL_IMAGE="http://image.tmdb.org/t/p/w342";
    String ES_LANGUAGE="&language=es";
    String APPEND="&append_to_response=";



    @GET(URL_SEARCH+ES_LANGUAGE)
    Call<Responses> searchMovie(@Query("query") String query);

    @GET(URL_MOVIE+"{id}"+URL_KEY+APPEND+URL_CREDITS+ES_LANGUAGE)
    Call<Movie> getMovie(@Path("id") int movieId);

    @GET(URL_MOVIE+"{id}"+URL_CREDITS+URL_KEY+ES_LANGUAGE)
    Call<Credits> getCredits(@Path("id") int movieId);

    @GET(URL_MOVIE+"{id}"+URL_RECOMMENDATIONS+URL_KEY+ES_LANGUAGE)
    Call<Responses> getRecommendations(@Path("id") int movieId);



}
