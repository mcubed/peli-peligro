package mmm.movies.Adapters;

import android.content.Context;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import mmm.movies.Beans.MovieCard;
import mmm.movies.Constants;
import mmm.movies.ClickListener;
import mmm.movies.R;


public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    private List<MovieCard> movies;
    private Context context;
    private ClickListener listener;


    public MovieAdapter(Context context, List<MovieCard> movies) {
        this.context = context;
        this.movies = movies;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.movie_cardview, viewGroup, false);
        return new MovieViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MovieViewHolder movieViewHolder, int i) {

        MovieCard current_movie=movies.get(i);

        movieViewHolder.setPosition(i);
        movieViewHolder.setMovie(current_movie);
        movieViewHolder.setMoviePoster(context,current_movie.getPoster_path());
        movieViewHolder.setStatus(current_movie.getStatus());

        movieViewHolder.setListener(listener);


    }
    public MovieCard getMovieCardAtPosition(int position){
        return movies.get(position);
    }
    public void setMovieCardAtPosition(MovieCard movie, int position){
        if(movie!=null)
            movies.set(position,movie);
    }
    public void removeMovie(int position){
        movies.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setListener(ClickListener listener){
        this.listener=listener;
    }


//    @Override
//    public void onClick(int viewId, int position) {
//        if(listener!=null)
//            listener.onClick(viewId,position);
//    }
//
//    @Override
//    public void onLongClick(int viewId, int position) {
//        if(listener!=null)
//            listener.onLongClick(viewId,position);
//    }


    public static class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        private int position;
        private TextView movieName;
        private TextView movieYear;
        private ImageView moviePoster;
        private ImageButton seen;
        private ImageButton wish;
        private CardView cardView;

        private ClickListener listener;


        MovieViewHolder(View itemView) {
            super(itemView);

            cardView = (CardView) itemView.findViewById(R.id.cv);
            movieName = (TextView)itemView.findViewById(R.id.movie_name);
            movieYear = (TextView)itemView.findViewById(R.id.movie_year);
            moviePoster = (ImageView)itemView.findViewById(R.id.movie_poster);
            seen = (ImageButton) itemView.findViewById(R.id.add_seen);
            wish = (ImageButton) itemView.findViewById(R.id.add_whish);

            cardView.setOnClickListener(this);
            seen.setOnClickListener(this);
            wish.setOnClickListener(this);
            cardView.setOnLongClickListener(this);

        }
        public void setMovie(MovieCard movie){
            movieName.setText(movie.getTitle());
            movieYear.setText(movie.getYear());
        }

        public void setMoviePoster(Context context, String poster_url){
            if(poster_url != null)
                Picasso.with(context).load(Constants.URL_IMAGE+poster_url).resize(90, 112).into(moviePoster);
        }

        public void setStatus(String status){
            if(status!=null) {
                seen.setSelected(status.equals(Constants.SEEN));
                wish.setSelected(status.equals(Constants.WISH));
            }
            else{
                seen.setSelected(false);
                wish.setSelected(false);
            }
        }
        public void setListener(ClickListener listener){
            this.listener=listener;
        }
        public void setPosition(int pos){
            this.position=pos;
        }


        @Override
        public void onClick(View view) {
            if(listener!=null){
                listener.onClick(view.getId(),position);
            }

        }

        @Override
        public boolean onLongClick(View view) {
            if(listener != null)
                listener.onLongClick(position);
            return false;
        }
    }
}
