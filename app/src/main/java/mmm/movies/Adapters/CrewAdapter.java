package mmm.movies.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import mmm.movies.Beans.Worker;
import mmm.movies.Constants;
import mmm.movies.R;

public class CrewAdapter extends RecyclerView.Adapter<CrewAdapter.CrewViewHolder>{

    private List<Worker> crew;
    private Context context;
    private View actorView;


    public CrewAdapter(Context context, List<Worker> crew) {
        this.context = context;
        this.crew = crew;
    }

    @Override
    public CrewViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.actor_cardview, viewGroup, false);
        this.actorView=view;
        return new CrewViewHolder(view);
    }


    @Override
    public void onBindViewHolder(CrewViewHolder actorViewHolder, int i) {

        Worker current_worker=crew.get(i);

        actorViewHolder.setWorker(current_worker);
        actorViewHolder.setWorkerPicture(context,current_worker.getProfilePath());

    }
    public Worker getWorkerAtPosition(int position){
        return crew.get(position);
    }
    public void setWorkerAtPosition(Worker actor, int position){
        if(actor!=null)
            crew.set(position,actor);
    }


    @Override
    public int getItemCount() {
        return crew.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public View getWorkerView(){
        return actorView;
    }



    public static class CrewViewHolder extends RecyclerView.ViewHolder{

        private TextView workerName;
        private TextView character;
        private ImageView workerPicture;



        CrewViewHolder(View itemView) {
            super(itemView);

            workerName = (TextView)itemView.findViewById(R.id.actor_name);
            character = (TextView)itemView.findViewById(R.id.character);
            workerPicture = (ImageView)itemView.findViewById(R.id.actor_picture);



        }
        public void setWorker(Worker worker){

            workerName.setText(worker.getName());
            character.setText(worker.getJob());
        }


        public void setWorkerPicture(Context context, String picture_url){
            if(picture_url != null)
                Picasso.with(context).load(Constants.URL_IMAGE+picture_url).resize(50, 70).into(workerPicture);
        }


    }
}