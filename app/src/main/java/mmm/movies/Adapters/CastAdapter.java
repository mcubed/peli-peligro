package mmm.movies.Adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import mmm.movies.Beans.Actor;
import mmm.movies.Constants;
import mmm.movies.R;

public class CastAdapter extends RecyclerView.Adapter<CastAdapter.CastViewHolder>{

    private List<Actor> cast;
    private Context context;
    private View actorView;


    public CastAdapter(Context context, List<Actor> cast) {
        this.context = context;
        this.cast = cast;
    }

    @Override
    public CastViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.actor_cardview, viewGroup, false);
        this.actorView=view;
        return new CastViewHolder(view);
    }


    @Override
    public void onBindViewHolder(CastViewHolder actorViewHolder, int i) {

        Actor current_actor=cast.get(i);

        actorViewHolder.setActor(current_actor);
        actorViewHolder.setActorPicture(context,current_actor.getProfilePath());

    }
    public Actor getActorAtPosition(int position){
        return cast.get(position);
    }
    public void setActorAtPosition(Actor actor, int position){
        if(actor!=null)
            cast.set(position,actor);
    }


    @Override
    public int getItemCount() {
        return cast.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public View getActorView(){
        return actorView;
    }



    public static class CastViewHolder extends RecyclerView.ViewHolder{

        private TextView actorName;
        private TextView character;
        private ImageView actorPicture;



        CastViewHolder(View itemView) {
            super(itemView);

            actorName = (TextView)itemView.findViewById(R.id.actor_name);
            character = (TextView)itemView.findViewById(R.id.character);
            actorPicture = (ImageView)itemView.findViewById(R.id.actor_picture);



        }
        public void setActor(Actor actor){

            actorName.setText(actor.getName());
            character.setText(actor.getCharacter());
        }


        public void setActorPicture(Context context, String picture_url){
            if(picture_url != null)
                Picasso.with(context).load(Constants.URL_IMAGE+picture_url).resize(50, 70).into(actorPicture);
        }


    }
}