package mmm.movies.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import mmm.movies.Beans.MovieCard;
import mmm.movies.ClickListener;
import mmm.movies.Constants;
import mmm.movies.R;

public class MovieSmallAdapter extends RecyclerView.Adapter<MovieSmallAdapter.MovieViewHolder>{

    private List<MovieCard> movies;
    private Context context;
    private View movieView;
    private ClickListener listener;


    public MovieSmallAdapter(Context context, List<MovieCard> movies) {
        this.context = context;
        this.movies = movies;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.movie_cardview_horizontal, viewGroup, false);
        this.movieView=view;
        return new MovieViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MovieViewHolder movieViewHolder, int i) {

        MovieCard current_movie=movies.get(i);

        movieViewHolder.setMovie(current_movie);
        movieViewHolder.setMoviePoster(context,current_movie.getPoster_path());
        movieViewHolder.setPosition(i);
        movieViewHolder.setListener(listener);



    }
    public MovieCard getMovieCardAtPosition(int position){
        return movies.get(position);
    }
    public void setMovieCardAtPosition(MovieCard movie, int position){
        if(movie!=null)
            movies.set(position,movie);
    }


    @Override
    public int getItemCount() {
        return movies.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public View getMovieView(){
        return movieView;
    }

    public void setListener(ClickListener listener){
        this.listener=listener;
    }




    public static class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView movieName;
        private ImageView moviePoster;
        private CardView cardView;

        private ClickListener listener;
        private int position;

        MovieViewHolder(View itemView) {
            super(itemView);

            movieName = (TextView)itemView.findViewById(R.id.movie_name);
            moviePoster = (ImageView)itemView.findViewById(R.id.movie_poster);
            cardView = (CardView) itemView.findViewById(R.id.cv);

            cardView.setOnClickListener(this);

        }
        public void setMovie(MovieCard movie){
            movieName.setText(movie.getTitle());
        }

        public void setMoviePoster(Context context, String poster_url){
            if(poster_url != null)
                Picasso.with(context).load(Constants.URL_IMAGE+poster_url).resize(100, 130).into(moviePoster);
        }

        public void setListener(ClickListener listener){
            this.listener = listener;
        }
        public void setPosition(int pos){
            this.position=pos;
        }


        @Override
        public void onClick(View view) {
            if(listener!=null){
                listener.onClick(position);
            }

        }

    }
}